import $ from 'jquery';
import ionRangeSlider from 'ion-rangeslider';
import magnificPopup from 'magnific-popup';
import select2 from 'select2';
import WareHousesSlider from './views/warehousesSlider';
import MobileReviewsSlider from './views/MobileReviewsSlider';
import WareHousesSliderMobile from './views/WareHousesSliderMobile';
import anchorScroll from './vendor/anchor';

$.fn.extend({
    animateCss: function (animationName) {
        let animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    }
});

class Application{
    constructor(){
        document.addEventListener('DOMContentLoaded', () => {
            this._initStylerSelect();
            new WareHousesSlider();
            new WareHousesSliderMobile();
            new MobileReviewsSlider();
            this._openBurger();
            this._playPopupVideo();
            this._initMap();
            this._calculatorInit();
            this._initTabsSlider();
            this._initTabSliderMobile();
            this._initMobileCalc();
            this._initTriggerAnimation();
            this._callBackFormOpen();
            this._ajaxSendForm();
        })
    }

    _calculatorInit() {
        let cost;
        let month;
        let sum;
        let imgContainerSrc;
        let nameCar;
        let $imgCar = $('.calculator__image-car img');
        let imgContainer = $('.calculator__image-container img');
        //let irsSingle = $('.calculator__range-group--square .irs-single');
        $('#squareInput').ionRangeSlider({
            type: "single",
            values: ['1m²', '7m²', '10m²', '15m²', '30m²'],
            from: 0,
            step: 1,
            grid: true,
            grid_snap: true,
            onStart: function (data) {
                switch(data.from){
                    case 0: cost = 1000; imgContainerSrc = '1m'; break;
                    case 1: cost = 2000; imgContainerSrc = '7m'; break;
                    case 2: cost = 3000; imgContainerSrc = '10m'; break;
                    case 3: cost = 4000; imgContainerSrc = '15m'; break;
                    case 4: cost = 5000; imgContainerSrc = '30m'; break;
                }
                //sum = cost * month;
                if(data.from <= 0){
                    nameCar = 'light-car';
                }else if(data.from <= 3){
                    nameCar = 'medium-car';
                }else{
                    nameCar = 'big-car';
                }
                $imgCar.fadeOut(100, function () {
                    $imgCar.attr('src', './images/'+nameCar+'.png').fadeIn(100);
                });
                $('.calculator__output span').text(cost);
                imgContainer.attr('src', './images/containers/'+imgContainerSrc+'.png');
            },
            onFinish: function (data) {
                if(data.from <= 0){
                    nameCar = 'light-car';
                }else if(data.from <= 3){
                    nameCar = 'medium-car';
                }else{
                    nameCar = 'big-car';
                }
                $imgCar.fadeOut(100, function () {
                    $imgCar.attr('src', './images/'+nameCar+'.png').fadeIn(100);
                });
            },
            onChange: function (data) {
                switch(data.from){
                    case 0: cost = 1000; imgContainerSrc = '1m'; break;
                    case 1: cost = 2000; imgContainerSrc = '7m'; break;
                    case 2: cost = 3000; imgContainerSrc = '10m'; break;
                    case 3: cost = 4000; imgContainerSrc = '15m'; break;
                    case 4: cost = 5000; imgContainerSrc = '30m'; break;
                }
                //sum = cost * month;

                $('.calculator__output span').text(cost);
                imgContainer
                    .fadeOut(100, function () {
                        imgContainer.attr('src', './images/containers/'+imgContainerSrc+'.png')
                            .fadeIn(100);
                    });
                let descr = $('.calculator__range-group--square .irs-single').text();
                $('.calculator__square-descr')
                    .fadeOut(50, function () {
                        $('.calculator__square-descr span').text(descr)
                            .fadeIn(50);
                    }).fadeIn(50);
            }
        });
    }

    _initMobileCalc() {
        let cost;
        let month = $('#timeRentSelect option:selected').val();
        let sum;
        let $selectSquare = $('#squareInputSelect');
        let $selectTime = $('#timeRentSelect');
        let imgContainerSrc;
        let imgContainer = $('.calculator__image-container img');
        let valueSquare = $selectSquare.find(":selected").val();
        let valueTime = $selectTime.find(":selected").val();
        $selectSquare.on('change', function () {
            let $this = $(this);
            console.log($this.find(":selected").val());
            valueSquare = $this.find(":selected").val();
            switch(valueSquare){
                case "0": cost = 1000; imgContainerSrc = '1m'; break;
                case "1": cost = 2000; imgContainerSrc = '6m'; break;
                case "2": cost = 3000; imgContainerSrc = '9m'; break;
                case "3": cost = 4000; imgContainerSrc = '12m'; break;
                case "4": cost = 5000; imgContainerSrc = '35m'; break;
            }
            sum = cost * valueTime;
            imgContainer.attr('src', './images/containers/'+imgContainerSrc+'.png');
            let descr = $('#squareInputSelect option:selected').text();
            $('.calculator__square-descr span').text(descr);
            $('.calculator__output span').text(sum);
        });
        $selectTime.on('change', function () {
            let $this = $(this);
            valueTime = $this.find(":selected").val();
            sum = cost * valueTime;
            imgContainer.attr('src', './images/containers/'+imgContainerSrc+'.png');
            $('.calculator__output span').text(sum);
        });
    }

    _initMap() {
        let clickedMarker;

        ymaps.ready(init);

        function init () {
            let myMap = new ymaps.Map('mapSection', {
                    center: [55.76, 37.64],
                    zoom: 10
                }, {
                    searchControlProvider: 'yandex#search'
                }),
                objectManager = new ymaps.ObjectManager({
                    // Чтобы метки начали кластеризоваться, выставляем опцию.
                    clusterize: false,
                    // ObjectManager принимает те же опции, что и кластеризатор.
                    gridSize: 32,
                    clusterDisableClickZoom: true
                });

            // Чтобы задать опции одиночным объектам и кластерам,
            // обратимся к дочерним коллекциям ObjectManager.
            //objectManager.objects.options.set('preset', 'islands#greenDotIcon');
            objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');
            myMap.geoObjects.add(objectManager);

            $.ajax({
                url: "data.json"
            }).done(function(data) {
                objectManager.add(data);
            });

            function onObjectEvent (e) {
                let objectId = e.get('objectId');
                if (e.get('type') == 'mouseenter') {
                    // Метод setObjectOptions позволяет задавать опции объекта "на лету".
                    objectManager.objects.setObjectOptions(objectId, {
                        iconImageHref: '../images/placeholder.png'
                    });
                } else if(e.get('type') == 'click') {
                    clickedMarker = e.get('objectId');
                    console.log(clickedMarker);
                    objectManager.objects.each(function (object) {
                        objectManager.objects.setObjectOptions( object.id, {iconImageHref: '../images/red-pacemark.png'});
                    });
                    if(clickedMarker){
                        objectManager.objects.setObjectOptions(clickedMarker, {
                            iconImageHref: '../images/placeholder.png'
                        });
                    }
                } else {
                    if( objectId != clickedMarker){
                        objectManager.objects.setObjectOptions(objectId, {
                            iconImageHref: '../images/red-pacemark.png'
                        });
                    }
                }
            }

            objectManager.objects.events.add(['mouseenter', 'mouseleave', 'click'], onObjectEvent);
        }
    }

    _playPopupVideo() {
        $('.video-block__play-popup').magnificPopup({
            type: 'iframe'
        });
    }

    _initTabsSlider() {
        let i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("photo-warehouses__tabs-content");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("photo-warehouses__tabs-link");
        let activeTabContent = document.querySelector('.photo-warehouses__tabs-link.active').getAttribute('data-tab');
        document.getElementById(activeTabContent).style.display="block";
        [].forEach.call(tablinks, tab => {
            tab.addEventListener('click', () =>
                this._clickTab(tab, tabcontent, tablinks))
        });
    }

    _clickTab(tab, tabcontent, tablinks) {
        for (let i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        for (let i = 0; i < tablinks.length; i++) {
            tablinks[i].classList.remove('active');
        }
        tab.classList.add('active');
        let idTabContent = tab.getAttribute('data-tab');
        document.getElementById(idTabContent).style.display='block';
    }

    _initTabSliderMobile() {
        let i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("photo-warehouses__mobile-tab-content");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("photo-warehouses__tabs-select");
        let activeTabContent = document.querySelector(".photo-warehouses__tabs-select option:checked").value;
        document.getElementById(activeTabContent).style.display="block";
        $(tablinks).on('change', (event) => {
            this._changeTabMobile(event.currentTarget,tabcontent,tablinks);
        })

    }

    _changeTabMobile(tab, tabcontent, tablinks){
        /*console.log(tab);
        console.log(tabcontent);*/
        for (let i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        let idTabContent = tab.value;
        //console.log(idTabContent);
        document.getElementById(idTabContent).style.display='block';
    }

    _initTriggerAnimation() {
        let $massAnimation = $('[data-animate]');
        let $window = $(window);
        $massAnimation.css('visibility', 'hidden');
        //VIEWPORT TRIGGER
        $window.on('scroll', function () {
            $massAnimation.each(function () {
                let $this = $(this);

                if($this.data('isAnimate')) return;

                if ( $window.scrollTop() + ($window.height() * 0.9 ) > $this.offset().top) {
                    let animation = $this.data('animate');
                    let delay = $this.data('delay');
                    $this.css('animation-delay', delay + 's');
                    $this.animateCss(animation);
                    $this.css('visibility', '');
                    $this.data('isAnimate', true)
                }
            });
        }).trigger('scroll');
    }

    _initStylerSelect() {
        let $select = $('.custom-select');
        $select.select2({
            width: '100%'
        });
    }

    _openBurger() {
        $('.header__burger').on('click', function(){
            let $this = $(this);
            let $mobileMenu = $('.header__nav--mobile');
            $this.toggleClass('open');
            $mobileMenu.toggleClass('open animated fadeInBottom');
        });
    }

    _callBackFormOpen(){
        $('.open-call-back').magnificPopup({
            type:'inline',
            mainClass: 'mfp-fade'
        });
    }

    _ajaxSendForm(){
        $('form').submit( function(e){
            let $thisForm = $(this);
            //let $formMethod = $thisForm.attr('method');
            $('.contact-form__loader-overlay').fadeIn('slow');
            // $.ajax({
            //     type: 'post',
            //     url: '../send.php',
            //     data: $thisForm.serialize()
            //
            // }).done(function() {
            //     $('.contact-form__loader-overlay').fadeOut('slow');
            //         $thisForm[0].reset();
            //         $thisForm.fadeOut('slow');
            //         $thisForm.parent().find('.modal-popup__name').fadeOut('fast');
            //         $thisForm.parent().find('.modal-popup__success').fadeIn('slow');
            //     setTimeout(function () {
            //          $thisForm.parent().find('.modal-popup__success').fadeOut('slow');
            //          $thisForm.parent().find('.modal-popup__name').fadeIn('slow');
            //          $thisForm.fadeIn('slow');
            //      }, 3000);
            //     console.log('done');
            // }).fail(function() {
            //     console.log('fail');
            // });
            $.ajax({
                type: 'post',
                url: '../send.php',
                data: $thisForm.serialize(),
                success: function(data) {
                    let response = JSON.parse(data);
                    if(response.hasOwnProperty('success')){
                        $('.contact-form__loader-overlay').fadeOut('slow');
                        $thisForm[0].reset();
                        $thisForm.fadeOut('slow');
                        $thisForm.parent().find('.modal-popup__name').fadeOut('fast');
                        $thisForm.parent().find('.modal-popup__success').fadeIn('slow');
                        setTimeout(function () {
                            $thisForm.parent().find('.modal-popup__success').fadeOut('slow');
                            $thisForm.parent().find('.modal-popup__name').fadeIn('slow');
                            $thisForm.fadeIn('slow');
                        }, 3000);
                    }else if(response.hasOwnProperty('error')){
                        $('.contact-form__loader-overlay').fadeOut('slow');
                        $thisForm.parent().find('.modal-popup__fail-recaptcha').fadeIn('fast');
                        setTimeout(function () {
                            $thisForm.parent().find('.modal-popup__fail-recaptcha').fadeOut('fast');
                        }, 3000)
                    }
                },
                error: function (data) {
                    $thisForm.parent().find('.modal-popup__success').html(data);
                    //console.log(data);
                }
            });
            //отмена действия по умолчанию для кнопки submit
            e.preventDefault();
        });
    }
}

new Application();
